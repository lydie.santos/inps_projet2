<!DOCTYPE html>
<html>
  <head>
    <title>Présentation du projet d'IPS</title>
    <meta charset="utf-8" />
    <link rel="stylesheet" type="text/css" href="core/fonts/mono.css"> 
    <link rel="stylesheet" type="text/css" href="core/css/animate.css"> 
    <link rel="stylesheet" type="text/css" href="core/css/style_core.css"> 
    <link rel="stylesheet" type="text/css" href="core/css/mermaid.css"> 
    <link rel="stylesheet" type="text/css" href="core/css/gitgraph.css"> 
    <link rel="stylesheet" type="text/css" href="core/css/style_ensiie.css"> 
    <link rel="stylesheet" type="text/css" href="core/css/katex.css"> 
    <link rel="stylesheet" type="text/css" href="core/css/asciinema-player.css"> 



  </head>
  <body>
    <textarea id="source" readonly>
class: titlepage

.title[Visualisation et optimisation du calcul d'une densité de probabilité
]

.subtitle[
A. Christolomme, L. Santos, E. Berrou - ENSIIE - 2023
]


???


---
layout: true
class: animated fadeIn middle numbers

.footnote[
A. Christolomme, L. Santos, E. Berrou - ENSIIE - 2023
]


---

class: toc top
# Sommaire

1. <strong>Organisation du projet</strong>
  1. Structure générale
  2. Chaine de compilation

2. <strong>Densité locale d'une particule</strong>
  1. Calcul d'une densité locale
  2. Simplification du calcul

3. <strong>Implémentation</strong>
  1. Les optimisations
  2. Résultats

4. <strong>Outils </strong>
  1. Git
  2. La documentation : Doxygen
  3. Les tests : Cxxtest

---

# Organisation du projet

## Structure générale

Arborescence des fichiers :

.alert.tree.hcenter[
inps_projet2
* src
    * basis.cpp
    * main.cpp
    * poly.cpp
    * plot.py
* headers
    * basis.h
    * poly.h
* bin
* obj
* test
    * test_basis.h
    * test_poly.h
* doc
]

---

# Organisation du projet

## Chaine de compilation 

.hcenter.shadow.w85[![](images/chaine_compil.png)]
---

# Organisation du projet

## Chaine de compilation : Le Makefile

Parler juste du truc ou on a galéré avec Alice

```Makefile
CC=g++
CFLAGS= -Wall -Wextra -std=c++11 -g
LIBS = -larmadillo
TARGET = bin/exec
OBJS = obj/main.o obj/poly.o obj/basis.o
CXXTEST = cxxtestgen
CXXTESTFLAGS = --error-printer
TARGET_TEST = bin/test
TESTS = tests/test_poly.h tests/test_basis.h
OBJS_TEST = tests/test.o obj/poly.o obj/basis.o

all: $(TARGET)

$(TARGET): $(OBJS)
	$(CC) $(LDFLAGS) $^ -o $@ $(LIBS)

obj/%.o: src/%.cpp headers/%.h
	$(CC) $(CXXFLAGS) -c $< -o $@

obj/main.o: src/main.cpp
	$(CC) $(CXXFLAGS) -c $< -o $@
```

---

# Organisation du projet

## Chaine de compilation : Le Makefile

```Makefile
#Tests

test: $(TARGET_TEST)
	rm -f ./test/test.cpp
	./bin/test

$(TARGET_TEST): $(OBJS_TEST)
	$(CC) $^ -o $@ $(LIBS) 

tests/test.o: tests/test.cpp
	$(CC) $(CFLAGS) -c $< -o $@

tests/test.cpp: $(TESTS)
	cxxtestgen --error-printer -o $@ $^





.PHONY: clean all
clean:
	rm -f ./obj/*.o
	rm -f ./tests/*.o
	rm -f ./bin/test
	rm -f ./bin/exec
	rm -f ./tests/test.cpp
```

---

# Densité locale d'une particule

## Calcul d'une densité locale

On cherche à calculer la <strong>densité locale d'une particule</strong> $\rho(\vec{r})$, où $\vec{r}$ est le vecteur position de la particule en coordonnées cylindriques. On utilise, pour cela, la formule :

`$$\rho (\vec{r}) = \sum_a\sum_b\rho_{ab}\psi_a(\vec{r})\psi_b^*(\vec{r})$$`

Or, chaque fonction de base peut être définie par un unique triplet $(m, n, n_z)$ d'entiers non négatifs qu'on appelle <strong>nombres quantiques</strong>. Ainsi, on peut écrire :

`$$\psi_a=\psi_{m_a, n_a, n_za}$$`

On obtient donc :

`$$ \rho (\vec{r}) = \sum_{m_a}\sum_{n_a}\sum_{n_{za}}\sum_{m_b}\sum_{n_b}\sum_{n_{zb}} \rho_{m_a,n_a,n_{za},m_b,n_b,n_{zb}}(\vec{r})\psi_{m_a, n_a, n_za}(\vec{r})\psi_{m_b, n_b, n_zb}^*(\vec{r})$$`

Avec : `$$
\psi_{m,n,n_z}(r_\perp, \theta, z) \equiv Z(z, n_z) . R(r_\perp, m, n) . e^{im\theta}
$$`

---

# Densité locale d'une particule

## Calcul d'une densité locale : Simplification

On sait que :`$$\rho_{ab} = \delta_{m_a, m_b} \rho_{ab}$$`

On peut donc simplifier et on obtient :
`$$ \rho (\vec{r}) = \sum_{m_a}\sum_{n_a}\sum_{n_{za}}\sum_{n_b}\sum_{n_{zb}} \rho_{m_a,n_a,n_{za},m_a,n_b,n_{zb}}(\vec{r})\psi_{m_a, n_a, n_za}(\vec{r})\psi_{m_a, n_b, n_zb}^*(\vec{r})$$`
`$$ \rho (\vec{r}) = S \rho_{m_a,n_a,n_{za},m_a,n_b,n_{zb}}(\vec{r})Z(z, n_{za}) . R(r_\perp, m_a, n_a).Z(z, n_{zb}) . R(r_\perp, m_a, n_b) . e^{i\theta(m_a-m_a)}$$`
`$$ \rho (\vec{r}) = S \rho_{m_a,n_a,n_{za},m_a,n_b,n_{zb}}(\vec{r})Z(z, n_{za}) . R(r_\perp, m_a, n_a).Z(z, n_{zb}) . R(r_\perp, m_a, n_b)$$`

Avec `$$S = \sum_{m_a}\sum_{n_a}\sum_{n_{za}}\sum_{n_b}\sum_{n_{zb}}$$`

Ainsi, <strong>$\rho$ ne dépend pas de $\theta$</strong> et <strong>ne prend que des valeurs réelles</strong>.


---
# Implémentation 

## Les optimisations : algorithme naif

```c++
arma::mat naive_result(Basis basis, arma::vec zVals, arma::vec rVals)
{
    arma::mat result = arma::zeros(rVals.n_elem, zVals.n_elem);
    int a = 0;
    int b = 0;
    for (int m = 0; m < basis.mMax; m++)
    for (int n = 0; n < basis.nMax(m); n++)
    for (int n_z = 0; n_z < basis.n_zMax(m, n); n_z++)
    {
        b = 0;
        for (int mp = 0; mp < basis.mMax; mp++)
        for (int np = 0; np < basis.nMax(mp); np++)
        for (int n_zp = 0; n_zp < basis.n_zMax(mp, np); n_zp++)
        {
            arma::mat psiA = basis.psi(m,  n,  n_z, zVals, rVals);
            arma::mat psiB = basis.psi(mp, np, n_zp, zVals, rVals);   
            
            result += psiA % psiB * rho(a,b);
            b++;
        }
        a++;
      }

    return(result);
}
```


---
# Implémentation 

## Les optimisations : première optimisation

<strong>`$$\rho_{ab} = \delta_{m_a, m_b} \rho_{ab}$$`</strong>

```c++
arma::mat first_opti(Basis basis, arma::vec zVals, arma::vec rVals)
{
    arma::mat result = arma::zeros(rVals.n_elem, zVals.n_elem);
    int a = 0;
    int b = 0;

    for (int m = 0; m < basis.mMax; m++)
    for (int n = 0; n < basis.nMax(m); n++)
    for (int n_z = 0; n_z < basis.n_zMax(m, n); n_z++)
    {
        arma::mat psiA = basis.psi(m,  n,  n_z, zVals, rVals);
        a = rho_i(m, n, n_z);
        
        for (int np = 0; np < basis.nMax(m); np++)
        for (int n_zp = 0; n_zp < basis.n_zMax(m, np); n_zp++)
        {
            arma::mat psiB = basis.psi(m, np, n_zp, zVals, rVals);
        
            b = rho_i(m, np, n_zp);
            result += (psiA % psiB) * rho(a,b);
        }
    }
    return(result);
}

```


---
# Implémentation 

## Les optimisations : deuxième optimisation

<strong>`$$\rho_{ab} = \rho_{ba}$$`</strong>

```c++
arma::mat second_opti(Basis basis, arma::vec zVals, arma::vec rVals)
{
    arma::mat result = arma::zeros(rVals.n_elem, zVals.n_elem);
    int a = 0;
    int b = 0;

    for (int m = 0; m < basis.mMax; m++)
    for (int n = 0; n < basis.nMax(m); n++)
    for (int n_z = 0; n_z < basis.n_zMax(m, n); n_z++)
    {
        arma::mat psiA = basis.psi(m,  n,  n_z, zVals, rVals);
        a = rho_i(m, n, n_z);
        
        for (int np = 0; np < basis.nMax(m); np++)
        for (int n_zp = 0; n_zp < basis.n_zMax(m, np); n_zp++)
        {
            b = rho_i(m, np, n_zp);

            if (a < b)
            {
              arma::mat psiB = basis.psi(m, np, n_zp, zVals, rVals);
              result += 2 * (psiA % psiB) * rho(a,b);
            }

            if (a==b)
            {
              arma::mat psiB = basis.psi(m, np, n_zp, zVals, rVals);
              result += (psiA % psiB) * rho(a,b);
            }
        }

    return(result);
}
```


---
# Implémentation 

## Les optimisations : troisième optimisation


```c++
arma::mat third_opti(Basis basis, arma::vec zVals, arma::vec rVals)
{
    arma::mat result = arma::zeros(rVals.n_elem, zVals.n_elem);
    int a = 0;
    int b = 0;
    for (int m = 0; m < basis.mMax; m++)
    for (int n = 0; n < basis.nMax(m); n++)
    for (int n_z = 0; n_z < basis.n_zMax(m, n); n_z++)
    {
        arma::mat psiA = basis.psi(m,  n,  n_z, zVals, rVals);
        a = rho_i(m, n, n_z);
        b = rho_i(m, 0, 0);
        for (int np = 0; np < n; np++) 
        for (int n_zp = 0; n_zp < basis.n_zMax(m, np); n_zp++)  
            {
                arma::mat psiB = basis.psi(m, np, n_zp, zVals, rVals);
                result += 2 * psiA % psiB * rho(a,b);
                b++;
            }
        for (int n_zp = 0; n_zp < n_z ; n_zp ++)
        {
            arma::mat psiB = basis.psi(m, n, n_zp, zVals, rVals);
            result += 2 * psiA % psiB * rho(a,b); //rho (a,b)=rho(b,a)
            b++;
        }
        // case n = np and n_z = n_zp
        result += psiA % psiA * rho(b,a);
    }
    return(result);
}
```


---
# Implémentation 

## Les optimisations : dernière optimisation


```c++
arma::mat sixth_opti(Basis basis, arma::vec zVals, arma::vec rVals)
{
    arma::mat result = arma::zeros(rVals.n_elem, zVals.n_elem);
    arma::cube rcube = basis.rPart_cube(rVals, basis.mMax-1, basis.nMax(0)-1);
    arma::mat zmat = basis.zPart_mat(zVals, basis.n_zMax(0, 0)-1);
    int a = 0;
    int b = 0;

    for (int m = 0; m < basis.mMax; m++)
    for (int n = 0; n < basis.nMax(m); n++)
    for (int n_z = 0; n_z < basis.n_zMax(m, n); n_z++)
    {
        arma::mat psiA = rcube.slice(n).col(m) * zmat.col(n_z).t();
        a = rho_i(m, n, n_z);
        b = rho_i(m, 0, 0);

        for (int np = 0; np < n; np++) 
        for (int n_zp = 0; n_zp < basis.n_zMax(m, np); n_zp++) 
        {
            arma::mat psiB = rcube.slice(np).col(m) * zmat.col(n_zp).t();
            result += 2 * psiA % psiB * rho(a,b);
            b++;
        }
        
        for (int n_zp = 0; n_zp < n_z ; n_zp ++)
        {
            arma::mat psiB = rcube.slice(n).col(m) * zmat.col(n_zp).t();
            result += 2 * psiA % psiB * rho(a,b); //rho (a,b)=rho(b,a)
            b++;
        }

        result += psiA % psiA * rho(b,a);
    }
    return(result);
}
```


---
# Implémentation

## Résultats : Affichage 2d

.hcenter.shadow.w95[![](images/visu2d.png)]

---
# Implémentation

## Résultats : Affichage 3d

.hcenter.shadow.w80[![](images/visu3d.png)]

---
# Implémentation

## Résultats : Speed-Up

<pre id="terminal">
Time for the naive algorith : 3.38816
Time for the first optimization : 0.273172
Time for the second optimization : 0.149351
Time for the third optimization : 0.146738
Time for the fourth optimization : 0.131696
Time for the fifth optimization : 0.0890333
Time for the last optimization : 0.0672762
SpeedUp : 5036.19
</pre>

---

# Les Outils

## Git

.hcenter.shadow.w95[![](images/git.png)]


---
# Les Outils

## La documentation

.hcenter.shadow.w85[![](images/doc1.png)]

---
# Les Outils

## La documentation

.hcenter.shadow.w60[![](images/doc2.png)]

---

# Les Outils

## Cxxtest : tests obligatoires

```c++
void test_mandatory_03()
{
    // Mandatory test #03 - Basis z-functions
    TS_TRACE("Starting basis z-functions test");
    //     br = 1.935801664793151, bz = 2.829683956491218, N = 14, Q = 1.3
    Basis basis(1.935801664793151,      2.829683956491218,     14,     1.3);
    
    arma::vec z = {-10.1, -8.4, -1.0, 0.0, 0.1, 4.3, 9.2, 13.7};
    
    arma::vec res00 = { 7.64546544834383e-04,
                        5.44886272162148e-03,
                        4.19492564268520e-01,
                        4.46522724110539e-01,
                        4.46243982300708e-01,
                        1.40736821086932e-01,
                        2.26186220733178e-03,
                        3.62929640195959e-06};
    
    TS_ASSERT_DELTA(arma::norm(basis.zPart(z, 0) - res00), 0.0, 1e-15);
    
    arma::vec res15 = {-9.48674551049192e-02,
                        -1.40338701953237e-03,
                        1.85620628040096e-01,
                        -0.00000000000000e+00,
                        -3.93028470685214e-02,
                        -1.79526868763440e-01,
                        2.15604096600475e-01,
                        2.44977220882127e-01};
    
    TS_ASSERT_DELTA(arma::norm(basis.zPart(z, 15) - res15), 0.0, 1e-15);
    TS_TRACE("Finishing basis z-functions test\n");
}
```


---

class: hcenter, middle
# Conclusion

** Conclusion**

 </textarea>

    <script src="core/javascript/remark.js"></script>
    <script src="core/javascript/katex.min.js"></script>
    <script src="core/javascript/auto-render.min.js"></script>
    <script src="core/javascript/emojify.js"></script>
    <script src="core/javascript/mermaid.js"></script>
    <script src="core/javascript/jquery-2.1.1.min.js"></script>
    <script src="core/javascript/extend-jquery.js"></script>
    <script src="core/javascript/gitgraph.js"></script>
    <script src="core/javascript/plotly.js"></script>
    <script src="core/javascript/asciinema-player.js"></script>
    <script src="core/javascript/bokeh-2.2.1.min.js"></script>
    <script src="core/javascript/bokeh-widgets-2.2.1.min.js"></script>
    <script src="core/javascript/bokeh-tables-2.2.1.min.js"></script>
    <script src="core/javascript/bokeh-api-2.2.1.min.js"></script>

    <script>

    // === Remark.js initialization ===
    var slideshow = remark.create(
    {
      highlightStyle: 'monokai',
      countIncrementalSlides: false,
      highlightLines: false
    });

    // === Mermaid.js initialization ===
    mermaid.initialize({
      startOnLoad: false,
      cloneCssStyles: false,
      flowchart:{
        height: 50
      },
      sequenceDiagram:{
        width: 110,
        height: 30
      }
    });

    function initMermaid(s) {
      var diagrams = document.querySelectorAll('.mermaid');
      var i;
      for(i=0;i<diagrams.length;i++){
        if(diagrams[i].offsetWidth>0){
          mermaid.init(undefined, diagrams[i]);
        }
      }
    }

    slideshow.on('afterShowSlide', initMermaid);
    initMermaid(slideshow.getSlides()[slideshow.getCurrentSlideIndex()]);

    
    // === Emojify.js initialization ===
    emojify.run();

    // KaTeX
    renderMathInElement(document.body,{delimiters: [{left: "$$", right: "$$", display: true}, {left: "$", right: "$", display: false}], ignoredTags: ["script", "noscript", "style", "textarea", "pre"] });

    
    // ===== END =====
    

    // ===== END =====

    </script>
    <script src="gitgraphs.js" type="text/javascript"></script>
  </body>
</html>

