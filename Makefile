CC=g++
CFLAGS= -Wall -Wextra -std=c++11 -g
LIBS = -larmadillo
TARGET = bin/exec
OBJS = obj/main.o obj/poly.o obj/basis.o
CXXTEST = cxxtestgen
CXXTESTFLAGS = --error-printer
TARGET_TEST = bin/test
TESTS = tests/test_poly.h tests/test_basis.h
OBJS_TEST = tests/test.o obj/poly.o obj/basis.o

all: $(TARGET)

$(TARGET): $(OBJS)
	$(CC) $(LDFLAGS) $^ -o $@ $(LIBS)

obj/%.o: src/%.cpp headers/%.h
	$(CC) $(CXXFLAGS) -c $< -o $@

obj/main.o: src/main.cpp
	$(CC) $(CXXFLAGS) -c $< -o $@


#Tests

test: $(TARGET_TEST)
	rm -f ./test/test.cpp
	./bin/test

$(TARGET_TEST): $(OBJS_TEST)
	$(CC) $^ -o $@ $(LIBS) 

tests/test.o: tests/test.cpp
	$(CC) $(CFLAGS) -c $< -o $@

tests/test.cpp: $(TESTS)
	cxxtestgen --error-printer -o $@ $^





.PHONY: clean all
clean:
	rm -f ./obj/*.o
	rm -f ./tests/*.o
	rm -f ./bin/test
	rm -f ./bin/exec
	rm -f ./tests/test.cpp