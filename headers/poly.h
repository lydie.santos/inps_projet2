/**
 * @file poly.h
*/

#ifndef POLY_h
#define POLY_h

#include <armadillo>

class Poly
{
    private:
        /**
         * @brief This object stores the computed values of hermit polynomials. A column matches one polynomial while a row matches a value on which polynomials are evaluated. 
         * 
         */
        arma::mat hermite_mat;

        /**
         * @brief This object stores the computed values of laguerre polynomials. A row matches a value on which polynomials are evaluated, a column is for one m value and a slice is for one n value.
         */
        arma::cube laguerre_cube; 

    public:
        Poly();

        /**
         * @brief Getter for the cube 
         * 
         * @return laguerre_cube
        */
        arma::cube GetCube();


        /**
         * @brief This function computes some hermit polynomials on a mesh and stores them in hermite_mat
         * 
         * @param number the number of hermit polynomials to compute (from 0 to number -1)
         * @param zVals the mesh on which the hermit polynomials are to be computed
         */
        void calcHermite(int number, arma::vec zVals);


        /**
         * @brief This function returns a vector of an hermit polynomial computed on a mesh (previously computed) 
         * 
         * @attention the method calcHermite has to be called before this one with 'number' >= 'n' + 1
         * @param n the index of the hermit polynomial
         * @return arma::vec corresponding the the nth hermith polynomial stored in hermite_mat
         */
        arma::vec hermite(int n);


        /**
         * @brief This function computes some laguerre polynomials on a mesh and stores them in laguerre_cube
         * 
         * @param m_number the number of m values (from 0 to m_number -1) 
         * @param n_number the number of n values (from 0 to n_number -1)
         * @param zVals the mesh on which the laguerre polynomials are to be computed
         */
        void calcLaguerre(int m_number, int n_number, arma::vec zVals);


        /**
         * @brief This function returns a vector of a laguerre polynomial computed on a mesh (previously computed)
         *
         * @attention the method calcLaguerre has to be called before this one with 'm_number' >= 'm' + 1 and 'n_number' >= 'n' + 1
         * @param m the m index of the laguerre polynomial
         * @param n the n index of the laguerre polynomial
         * @return arma::vec corresponding to the laguerre polynomials with index m and n stored in laguerre_cube
         */
        arma::vec laguerre(int m, int n);

};

#endif