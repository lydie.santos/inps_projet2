/**
 * @file main.cpp
*/

#include "../headers/basis.h"
#include <iostream>




arma::mat rho;
arma::icube rho_i;


/**
 * @brief Function that write the result of the matrix in a DF3 file
 *
 * @param m matrix with the result to store in the file
 * @return what the df3 file must contain to plot m
 */
std::string cubeToDf3(const arma::cube &m)
{
  std::stringstream ss(std::stringstream::out | std::stringstream::binary);
  int nx = m.n_rows;
  int ny = m.n_cols;
  int nz = m.n_slices;
  ss.put(nx >> 8);
  ss.put(nx & 0xff);
  ss.put(ny >> 8);
  ss.put(ny & 0xff);
  ss.put(nz >> 8);
  ss.put(nz & 0xff);
  double theMin = 0.0;
  double theMax = m.max();
  for (uint k = 0; k < m.n_slices; k++)
  {
    for (uint j = 0; j < m.n_cols; j++)
    {
      for (uint i = 0; i < m.n_rows; i++)
      {
        uint v = 255 * (fabs(m(i, j, k)) - theMin) / (theMax - theMin);
        ss.put(v);
      }
    }
  }
  return ss.str();
}


/**
 * @details implementation of the naive algorithm given in the project instructions
*/
arma::mat naive_result(Basis basis, arma::vec zVals, arma::vec rVals)
{
    arma::mat result = arma::zeros(rVals.n_elem, zVals.n_elem);
    int a = 0;
    int b = 0;
    for (int m = 0; m < basis.mMax; m++)
    {
        for (int n = 0; n < basis.nMax(m); n++)
        {
            for (int n_z = 0; n_z < basis.n_zMax(m, n); n_z++)
            {
                b = 0;
                for (int mp = 0; mp < basis.mMax; mp++)
                {
                    for (int np = 0; np < basis.nMax(mp); np++)
                    {
                        for (int n_zp = 0; n_zp < basis.n_zMax(mp, np); n_zp++)
                        {
                            arma::mat psiA = basis.psi(m,  n,  n_z, zVals, rVals);
                            arma::mat psiB = basis.psi(mp, np, n_zp, zVals, rVals);   
                            
                            result += psiA % psiB * rho(a,b);
                            b++;

                            //result += funcA % funcB * rho(m, n, n_z, mp, np, n_zp); // mat += mat % mat * double
                        }
                    }
                }
                a++;
            }
        }
    }

    return(result);
}


/**
 * @details first optimisation of the naive algorithm
 * 
 * -> funcA calculated after the 3rd loop because does not depend of b
 * -> mp = m because rho(a,b)=delta(m_a,m_b)rho(a,b). That implies m_a=m_b
*/
arma::mat first_opti(Basis basis, arma::vec zVals, arma::vec rVals)
{
    arma::mat result = arma::zeros(rVals.n_elem, zVals.n_elem);
    int a = 0;
    int b = 0;

    for (int m = 0; m < basis.mMax; m++)
    {
        for (int n = 0; n < basis.nMax(m); n++)
        {
            for (int n_z = 0; n_z < basis.n_zMax(m, n); n_z++)
            {
                arma::mat psiA = basis.psi(m,  n,  n_z, zVals, rVals);
                a = rho_i(m, n, n_z);
                
                //p_ab = d_ma,mb*pab => mp = m
                for (int np = 0; np < basis.nMax(m); np++)
                {
                    for (int n_zp = 0; n_zp < basis.n_zMax(m, np); n_zp++)
                    {
                        arma::mat psiB = basis.psi(m, np, n_zp, zVals, rVals);
                    
                        b = rho_i(m, np, n_zp);
                        result += (psiA % psiB) * rho(a,b);
                    }
                }
            }
        }
    }

    return(result);
}


/**
 * @details second optimisation of the naive algorithm
 * 
 * -> symetry of rho : rho(a,b) = rho(b,a)
*/
arma::mat second_opti(Basis basis, arma::vec zVals, arma::vec rVals)
{
    arma::mat result = arma::zeros(rVals.n_elem, zVals.n_elem);
    int a = 0;
    int b = 0;

    for (int m = 0; m < basis.mMax; m++)
    {
        for (int n = 0; n < basis.nMax(m); n++)
        {
            for (int n_z = 0; n_z < basis.n_zMax(m, n); n_z++)
            {
                arma::mat psiA = basis.psi(m,  n,  n_z, zVals, rVals);
                a = rho_i(m, n, n_z);
                
                for (int np = 0; np < basis.nMax(m); np++)
                {
                    for (int n_zp = 0; n_zp < basis.n_zMax(m, np); n_zp++)
                    {
                        b = rho_i(m, np, n_zp);

                        if (a<b)
                        {
                          arma::mat psiB = basis.psi(m, np, n_zp, zVals, rVals);
                          result += 2 * (psiA % psiB) * rho(a,b); //because of the symetry
                        }

                        if (a==b)
                        {
                          arma::mat psiB = basis.psi(m, np, n_zp, zVals, rVals);
                          result += (psiA % psiB) * rho(a,b);
                        }
                        
                    }
                }
            }
        }
    }

    return(result);
}


/**
 * @details third optimisation of the naive algorithm
 * 
 * -> no useless loop, no if statements
*/
arma::mat third_opti(Basis basis, arma::vec zVals, arma::vec rVals)
{
    arma::mat result = arma::zeros(rVals.n_elem, zVals.n_elem);
    int a = 0;
    int b = 0;

    for (int m = 0; m < basis.mMax; m++)
    {
        for (int n = 0; n < basis.nMax(m); n++)
        {
            for (int n_z = 0; n_z < basis.n_zMax(m, n); n_z++)
            {
                arma::mat psiA = basis.psi(m,  n,  n_z, zVals, rVals);
                a = rho_i(m, n, n_z);
                b = rho_i(m, 0, 0);

                for (int np = 0; np < n; np++) 
                {
                    for (int n_zp = 0; n_zp < basis.n_zMax(m, np); n_zp++)  
                    {
                        arma::mat psiB = basis.psi(m, np, n_zp, zVals, rVals);
                        result += 2 * psiA % psiB * rho(a,b);
                        b++;
                    } // n_zp

                } // np 
                //for np = n
                for (int n_zp = 0; n_zp < n_z ; n_zp ++)
                {
                    arma::mat psiB = basis.psi(m, n, n_zp, zVals, rVals);
                    result += 2 * psiA % psiB * rho(a,b); //rho (a,b)=rho(b,a)
                    b++;
                }
                // case n = np and n_z = n_zp
                
                result += psiA % psiA * rho(b,a);
            }
        }
    }

    return(result);
}


/**
 * @brief fourth opti of the naive algorithm; variation of the fourth, less effective
 *
 * -> computing the r factors of psi functions only once, before the loops
 */   
arma::mat fourth_opti(Basis basis, arma::vec zVals, arma::vec rVals)
{
    arma::mat result = arma::zeros(rVals.n_elem, zVals.n_elem);

    // calculer et stocker les rPart et zPart en amont et toutes d'un coup pour éviter les récurrences inutiles
    arma::cube rcube = basis.rPart_cube(rVals, basis.mMax-1, basis.nMax(0)-1);

    int a = 0;
    int b = 0;

    for (int m = 0; m < basis.mMax; m++)
    {
        for (int n = 0; n < basis.nMax(m); n++)
        {
            for (int n_z = 0; n_z < basis.n_zMax(m, n); n_z++)
            {
                arma::mat psiA = rcube.slice(n).col(m) * basis.zPart(zVals,n_z).t();
                a = rho_i(m, n, n_z);
                b = rho_i(m, 0, 0);

                for (int np = 0; np < n; np++) 
                {
                    for (int n_zp = 0; n_zp < basis.n_zMax(m, np); n_zp++) 
                    {
                        arma::mat psiB = rcube.slice(np).col(m) * basis.zPart(zVals,n_zp).t();
                        result += 2 * psiA % psiB * rho(a,b);
                        b++;
                    } // n_zp

                } // np 
                //for np = n
                for (int n_zp = 0; n_zp < n_z ; n_zp ++)
                {
                    arma::mat psiB = rcube.slice(n).col(m) * basis.zPart(zVals,n_zp).t();
                    result += 2 * psiA % psiB * rho(a,b); //rho (a,b)=rho(b,a)
                    b++;
                }
                // case n = np and n_z = n_zp
                
                result += psiA % psiA * rho(b,a);
            }
        }
    }

    return(result);
}


/**
 * @brief fifth opti of the naive algorithm; variation of the fourth, less effective
 *
 * -> computing the z factors of psi functions only once, before the loops
 */   
arma::mat fifth_opti(Basis basis, arma::vec zVals, arma::vec rVals)
{
    arma::mat result = arma::zeros(rVals.n_elem, zVals.n_elem);
    arma::mat zmat = basis.zPart_mat(zVals, basis.n_zMax(0, 0)-1);

    int a = 0;
    int b = 0;

    for (int m = 0; m < basis.mMax; m++)
    {
        for (int n = 0; n < basis.nMax(m); n++)
        {
            for (int n_z = 0; n_z < basis.n_zMax(m, n); n_z++)
            {
                arma::mat psiA =  basis.rPart(rVals,m,n) * zmat.col(n_z).t();
                a = rho_i(m, n, n_z);
                b = rho_i(m, 0, 0);

                for (int np = 0; np < n; np++) 
                {
                    for (int n_zp = 0; n_zp < basis.n_zMax(m, np); n_zp++) 
                    {
                        arma::mat psiB =  basis.rPart(rVals,m,np) * zmat.col(n_zp).t();
                        result += 2 * psiA % psiB * rho(a,b);
                        b++;
                    } // n_zp

                } // np 
                //for np = n
                for (int n_zp = 0; n_zp < n_z ; n_zp ++)
                {
                    arma::mat psiB = basis.rPart(rVals,m,n) * zmat.col(n_zp).t();
                    result += 2 * psiA % psiB * rho(a,b); //rho (a,b)=rho(b,a)
                    b++;
                }
                // case n = np and n_z = n_zp
                
                result += psiA % psiA * rho(b,a);
            }
        }
    }

    return(result);
}


/**
 * @brief sixth opti of the naive algorithm
 *
 * -> computing all required factors of psi functions only once, before the loops
 */
arma::mat sixth_opti(Basis basis, arma::vec zVals, arma::vec rVals)
{
    arma::mat result = arma::zeros(rVals.n_elem, zVals.n_elem);
    arma::cube rcube = basis.rPart_cube(rVals, basis.mMax-1, basis.nMax(0)-1);
    arma::mat zmat = basis.zPart_mat(zVals, basis.n_zMax(0, 0)-1);
    int a = 0;
    int b = 0;

    for (int m = 0; m < basis.mMax; m++)
    {
        for (int n = 0; n < basis.nMax(m); n++)
        {
            for (int n_z = 0; n_z < basis.n_zMax(m, n); n_z++)
            {
                arma::mat psiA = rcube.slice(n).col(m) * zmat.col(n_z).t();
                a = rho_i(m, n, n_z);
                b = rho_i(m, 0, 0);

                for (int np = 0; np < n; np++) 
                {
                    for (int n_zp = 0; n_zp < basis.n_zMax(m, np); n_zp++) 
                    {
                        arma::mat psiB = rcube.slice(np).col(m) * zmat.col(n_zp).t();
                        result += 2 * psiA % psiB * rho(a,b);
                        b++;
                    } // n_zp

                } // np 
                //for np = n
                for (int n_zp = 0; n_zp < n_z ; n_zp ++)
                {
                    arma::mat psiB = rcube.slice(n).col(m) * zmat.col(n_zp).t();
                    result += 2 * psiA % psiB * rho(a,b); //rho (a,b)=rho(b,a)
                    b++;
                }
                // case n = np and n_z = n_zp
                result += psiA % psiA * rho(b,a);
            }
        }
    }

    return(result);
}


/**
 * @brief Method that calculates the symetry aroung the z axis
 * 
 * @param basis the basis used
 * @param p 2d result matrix
 * @return the 3d result
*/
arma::cube result3d(Basis basis, arma::mat p)
{
  //We set the dimensions
  arma::cube m = arma::cube(32, 32, 64);

  for (int x = 0; x < 32; x++) //x between 0 and 32 => center in (16,16)
  {
    for (int y = 0; y < 32; y++)
    {
      //We calculate the distance between x and y to set the point afterward
      int dist = (int)std::sqrt(std::pow(x - 16, 2) + std::pow(y - 16, 2));

      for (int z = 0; z < 64; z++)
      {
        if(dist < 16) //We are outside the cube so the density is null
        {
          //If we are in the center
          if(dist <= 0) 
          {
            m(x,y,z) = 0;
          }

          else
          {
            m(x,y,z) = 0.5 * (p(dist - 1, z) + p(dist, z));
          } 
        }

        else
        {
          m(x, y, z) = 0;
        }
      }
    }
  }

  return m;
}



int main()
{   
    Basis basis(1.935801664793151,      2.829683956491218,     14,     1.3);
    arma::vec zVals = arma::linspace(-20, 20, 64);
    arma::mat rVals = arma::linspace(0, 10, 16);



    /**
     * Construction of the matrix rho_i with rho
    */
    rho.load("rho.arma", arma::arma_ascii);

    rho_i = arma::icube(basis.mMax, basis.nMax(0), basis.n_zMax(0, 0));
    uint i = 0;
    for (int m = 0; m < basis.mMax; m++)
    for (int n = 0; n < basis.nMax(m); n++)
    for (int n_z = 0; n_z < basis.n_zMax(m, n); n_z++)
    {
      rho_i(m, n, n_z) = i;
      i++;
    }



    /**
     * Determination of times & speedUp
    */
    arma::wall_clock clock;
    clock.tic(); //We start the clock
    arma::mat res = naive_result(basis, zVals, rVals);
    double naive_duration = clock.toc();
    std::cout << "Time for the naive algorith : " << naive_duration << std::endl; //We print the result
    

    clock.tic(); //We start the clock
    arma::mat res1 = first_opti(basis, zVals, rVals);
    double first_duration = clock.toc();
    std::cout << "Time for the first optimization : " << first_duration << std::endl; //We print the result

    clock.tic(); //We start the clock
    arma::mat res2 = second_opti(basis, zVals, rVals);
    double second_duration = clock.toc();
    std::cout << "Time for the second optimization : " << second_duration << std::endl; //We print the result

    
    clock.tic(); //We start the clock
    arma::mat res3 = third_opti(basis, zVals, rVals);
    double third_duration = clock.toc();
    std::cout << "Time for the third optimization : " << third_duration << std::endl; //We print the result
    
    clock.tic(); //We start the clock
    arma::mat res4 = fourth_opti(basis, zVals, rVals);
    double fourth_duration = clock.toc();
    std::cout << "Time for the fourth optimization : " << fourth_duration << std::endl; //We print the result

    clock.tic(); //We start the clock
    arma::mat res5 = fifth_opti(basis, zVals, rVals);
    double fifth_duration = clock.toc();
    std::cout << "Time for the fifth optimization : " << fifth_duration << std::endl; //We print the result

     clock.tic(); //We start the clock
    arma::mat res6 = sixth_opti(basis, zVals, rVals);
    double sixth_duration = clock.toc();
    std::cout << "Time for the sixth optimization : " << sixth_duration << std::endl; //We print the result

    std::cout << "SpeedUp : " << naive_duration/sixth_duration * 100 << std::endl;;



    /**
     * 2D Plot
     */
    /*
    res.save("resu.csv", arma::csv_ascii);
    res1.save("resu1.csv", arma::csv_ascii);
    res2.save("resu2.csv", arma::csv_ascii);
    res3.save("resu3.csv", arma::csv_ascii);
    res4.save("resu4.csv", arma::csv_ascii);
    res5.save("resu5.csv", arma::csv_ascii);
    res6.save("resu6.csv", arma::csv_ascii);
    */



    /**
     * 3D Plot
    */
    arma::cube cube = result3d(basis, res4);
    std::string myFile = cubeToDf3(cube);
    std::ofstream out("Wow.df3");
    out << myFile;
    out.close();

    

    return 0;
}