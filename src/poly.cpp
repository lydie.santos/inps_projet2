/**
 * @file poly.cpp
*/

#include "../headers/poly.h"

Poly::Poly()
{

}


/**
 * @brief Getter for the cube 
 * 
 * @return laguerre_cube
*/
arma::cube Poly::GetCube()
{
    return laguerre_cube;
}


/**
 * @brief This function computes some hermit polynomials on a mesh and stores them in hermite_mat
 * 
 * @param number the number of hermit polynomials to compute (from 0 to number -1)
 * @param zVals the mesh on which the hermit polynomials are to be computed
*/
void Poly::calcHermite(int number, arma::vec zVals) //nb_rows is the number of columns ...
{
    //Initialization of the matrix with 1
    hermite_mat.arma::mat::ones(zVals.n_elem, number);
    
    //hermite_mat_1(z)
    if (number > 1)
    {
    hermite_mat.col(1) = 2 * hermite_mat.col(1) % zVals; // = 2* zVals;
    }

    //hermite_mat_{n+1}(z)
    for (int n = 1; n < number - 1; n++)  //n will reach nb_level - 2 which means we wil have nb_level of energy, up the (nb_level -1)th 
    {
        hermite_mat.col(n+1) = 2 * zVals % hermite_mat.col(n) - 2 * n * hermite_mat.col(n-1);
    }
}


/**
 * @brief This function returns a vector of an hermit polynomial computed on a mesh (previously computed) 
 * 
 * @attention the method calcHermite has to be called before this one with 'number' >= 'n' + 1
 * @param n the index of the hermit polynomial
 * @return arma::vec corresponding the the nth hermith polynomial stored in hermite_mat
*/
arma::vec Poly::hermite(int n)
{
    return hermite_mat.col(n);
}


/**
 * @brief This function computes some laguerre polynomials on a mesh and stores them in laguerre_cube
 * 
 * @param m_number the number of m values (from 0 to m_number -1) 
 * @param n_number the number of n values (from 0 to n_number -1)
 * @param zVals the mesh on which the laguerre polynomials are to be computed
*/
void Poly::calcLaguerre(int m_number, int n_number, arma::vec zVals)
{
    //Initialization of the matrix with 1
    laguerre_cube.arma::cube::ones(zVals.n_elem, m_number, n_number); //row, col, slice


    //laguerre_cube_1(n)
    if (n_number > 1){
    for (int m = 0; m < m_number ; m++)
    {
        laguerre_cube.slice(1).col(m) += m - zVals;
    }
    }

    //laguerre_cube_{n+1}(n)
    for (int n = 2; n < n_number ; n++)
    {
        for (int m = 0; m < m_number; m++)
        {
            laguerre_cube.slice(n).col(m) = (2 + (m - 1 - zVals)/n) % laguerre_cube.slice(n-1).col(m) - (double)(1 + (m - 1.)/n) * laguerre_cube.slice(n-2).col(m);
        }
    }
}


/**
 * @brief This function returns a vector of a laguerre polynomial computed on a mesh (previously computed)
 *
 * @attention the method calcLaguerre has to be called before this one with 'm_number' >= 'm' + 1 and 'n_number' >= 'n' + 1
 * @param m the m index of the laguerre polynomial
 * @param n the n index of the laguerre polynomial
 * @return arma::vec corresponding to the laguerre polynomials with index m and n stored in laguerre_cube
*/
arma::vec Poly::laguerre(int m, int n)
{
    return laguerre_cube.slice(n).col(m);
}
