\mainpage Inps Projet 2
# Visualisation et optimisation du calcul d'une densité de probabilité


The purpose of this project is to print in 3 dimensions a probability density and to optimize the implementation to obtain a code running which is the fastest possible


## How to use

> make

> ./bin/exec

> python3 ./src/plot.py



