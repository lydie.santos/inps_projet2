import numpy as np #for array
import matplotlib.pyplot as plt #for the print function


mat = np.loadtxt('resu.csv', delimiter= ',', dtype=float)

plt.matshow(mat)

plt.title("Densité local en y = 0") #title of the graph
plt.xlabel("z") #name of the x-axis
plt.ylabel("x") #name of the y-axis

plt.show() #print the graph