/**
 * @file basis.cpp
*/
#include <math.h>

#include "../headers/basis.h"
/**
 * @brief Construct a new Basis object
 * 
 * @param br basis deformation parameter br
 * @param bz basis deformation parameter bz
 * @param N basis truncation parameter N
 * @param Q basis truncation parameter Q
 *
 */
Basis::Basis(double my_br, double my_bz, int N, double Q)
{
    br = my_br;
    bz = my_bz;
    // computation of mMax
    mMax = (int) ((N + 2) * pow(Q, -1./3) - 0.5/Q); 
    
    //initialization of nMax and n_zMax
    nMax = arma::ivec(mMax).zeros();
    n_zMax = arma::imat(mMax, (int) ((0.5) * (mMax - 1) + 1)).zeros();

    for (int m = 0 ; m < mMax ; m++)
    {
        nMax(m) = (int) ((0.5)*(mMax - m - 1) + 1);
        for (int n = 0; n < nMax(m); n++)
        {
            n_zMax(m,n) = (int) ((N+2) * pow(Q, 2./3) + 0.5 - (m + 2*n + 1) * Q);
        }
    }
    
}

/**
 * @brief Function that calculate the exponential part in the determination of rPart and zPart
 * 
 * @param z the points where the function is evaluate
 * @param b the basis deformation parameter corresponding for the calcul
 * @return an arma::vec of the value for the exponential part of the solution
*/

arma::vec Basis::vec_exp(arma::vec z, double b)
{
    arma::vec E = -z % z/(2 * b * b);
    return arma::exp(E);
}

/**
 * @brief Function that returns the R factor for all possible values of m and n under the given ones (from 0 to m and 0 to n included)
 * @param r arma:::vec of the points the function is evaluated on
 * @param m int quantum number m maximal value
 * @param n int quantum number n maximal value
 *
 * @return the arma::cube of the R factor of the matching psi function
 */
arma::cube Basis::rPart_cube(arma::vec r, int m, int n)
{
    arma::cube rcube;
    rcube.arma::cube::ones(r.n_elem, m+1, n+1);
    double C; // sqrt(fact(n))/(br * sqrt(arma::datum::pi * (fact(n + m)))); 
    arma::vec D;
    D.arma::vec::ones(r.n_elem);
    arma::vec E = vec_exp(r, br);
    Poly P;
    P.calcLaguerre(m + 1, n + 1, r % r/(br * br)); //calculates even for values inferior to the one desired
    for (int j = 0; j <= n ; j++)
    {
        C = 1./(br * sqrt(arma::datum::pi));
        rcube.slice(j).col(0) = C * D % E %  P.laguerre(0, j);
    }
    double fa_j;
    double fa_i = 1;
    for (int i = 1 ; i <= m ; i++)
    {
        D = D % (r/br);
        fa_i *= 1./sqrt (i);
        for (int j = 0 ; j <= n ; j++)
        {
            if ( j == 0 )
            {
                 fa_j = fa_i;
            }

            else
            {
                fa_j *= sqrt ((double) j / (double) (j + i));
            }
            
            //C = sqrt(fact(j))/(br * sqrt(arma::datum::pi * (fact(j + i))));
            rcube.slice(j).col(i) = (fa_j * C) * D % E % P.laguerre(i, j);
        }
    }
    return rcube;
}


/**
 * @brief Function that returns the R factor with the given arguments of the psi function
 * 
 * @param r arma:::vec of the points the function is evaluated on
 * @param m int quantum number m
 * @param n int quantum number n
 * @return the arma::vec of the R factor of the matching psi function
 */
arma::vec Basis::rPart(arma::vec r, int m, int n)
{
    double C = sqrt(fact(n))/(br * sqrt(arma::datum::pi * (fact(n + m)))); // m is already positive
    arma::vec D = arma::pow (r/br, m);
    arma::vec E = vec_exp(r, br);
    Poly P;
    P.calcLaguerre(m + 1, n + 1, r % r/(br * br)); //calculates even for values inferior to the one desired
    arma::vec L = P.laguerre(m, n);
    return C * D % E % L;
}


/**
 * @brief Function that returns the Z factor with the given arguments of the psi function
 *
 * @param z arma:::vec of the points the function is evaluated on
 * @param nz int quantum number nz
 * @return the arma::vec of the Z factor of the matching psi function
 */
arma::vec Basis::zPart(arma::vec z, int nz)
{
    double C = sqrt(bz * (double) sqrt(arma::datum::pi) * pow (2, nz) * (long int) fact(nz));
    arma::vec E = vec_exp(z, bz);
    Poly P;
    P.calcHermite(nz + 1, z/bz); //calculates even for values inferior to the one desired
    arma::vec H = P.hermite(nz);
    return 1./C * E % H;
}

/**
 * @brief Function that returns the Z factors for all possible values of n_z under the given one (0 to nz included)
 * 
 * @param z arma:::vec of the points the function is evaluated on
 * @param nz int quantum number nz maximal value
 *
 * @return the arma::mat of the Z factors of the psi functions
 */
arma::mat Basis::zPart_mat(arma::vec z, int nz)
{
    arma::mat zmat;
    zmat.arma::mat::ones(z.n_elem, nz+1);
    double C = sqrt(bz * (double) sqrt(arma::datum::pi));
    arma::vec E = vec_exp(z, bz);
    Poly P;
    P.calcHermite(nz + 1, z/bz); //calculates even for values inferior to the one desired
    //i=0
    arma::vec H = P.hermite(0);
    zmat.col(0) = 1./C * E % H;
    for (int i=1; i <= nz ; i++)
    {
        C *= sqrt(2*i);
        arma::vec H = P.hermite(i);
        zmat.col(i) = 1./C * E % H;
    }
    
    return zmat;
}

/**
 * @brief Fonction that calculates n factorial
 * 
 * @param n the factorial
 * @return n!
*/
long long int Basis::fact(int n)
{
    long long int f = 1;
    for (int i = 1; i <= n; i++)
    {
        f *= i;
    }
    return f;

}


/**
 * @brief Function that returns a matrix of values of a psi function
 * 
 * @param m quatum number m, parameter of the psi function
 * @param n quantum number n, parameter of the psi function
 * @param nz quantum number nz, parameter of the psi function
 * @param z vector of the z values the psi function will be computed on; cylindrical basis parameter
 * @param r vector of the r values the psi function will be computed on; cylindrical basis parameter
 * @return the arma::mat of the values of the psi function with parameters m, n and nz computed on all pairs of values of z and r vectors
 */
arma::mat Basis::psi(int m, int n, int nz, arma::vec z, arma::vec r)
{
    arma::mat psi=arma::mat(z.n_elem,r.n_elem).zeros();
    psi = rPart(r,m,n) * zPart(z,nz).t() ;
    return(psi);
}
