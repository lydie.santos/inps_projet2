/**
 * @file basis.h
*/

#ifndef BASIS_H
#define BASIS_H

#include "poly.h"


class Basis
{
    public:
        int mMax; //int quantum number m maximal value
        arma::ivec nMax; //vector of the possible maximal value of quantum number n depending on m 
        arma::imat n_zMax; //matrix of the possible maximal value of quatum number nz depending on m and n
        double br; //basis deformation parameter
        double bz; //basis deformation parameter

    public:
        /**
         * @brief Construct a new Basis object
         * 
         * @param br basis deformation parameter br
         * @param bz basis deformation parameter bz
         * @param N basis truncation parameter N
         * @param Q basis truncation parameter Q
         */
        Basis(double br, double bz, int N, double Q);


        /**
         * @brief Function that returns the R factor with the given arguments of the psi function
         * 
         * @param r arma:::vec of the points the function is evaluated on
         * @param m int quantum number m
         * @param n int quantum number n
         * @return the arma::vec of the R factor of the matching psi function
         */
        arma::vec rPart(arma::vec r, int m, int n);


        /**
         * @brief Function that returns the Z factor with the given arguments of the psi function
         * 
         * @param z arma:::vec of the points the function is evaluated on
         * @param nz int quantum number nz
         * @return the arma::vec of the Z factor of the matching psi function
         */
        arma::vec zPart(arma::vec z, int nz);


        /**
         * @brief Fonction that calculates n factorial
         * 
         * @param n the factorial
         * @return n!
        */
        long long int fact(int n);


        /**
         * @brief Function that returns the vector of exp(-2*Z²/2b²) 
        * 
        * @param Z arma::vec of the points the function will be evaluated on
        * @param b either br or bz 
        * @return the arma::vec of the exponential factor of the rPart or zPart
        */
        arma::vec vec_exp(arma::vec Z,double b);


        /**
         * @brief Function that returns a matrix of values of a psi function
         * 
         * @param m quatum number m, parameter of the psi function
         * @param n quantum number n, parameter of the psi function
         * @param nz quantum number nz, parameter of the psi function
         * @param z vector of the z values the psi function will be computed on; cylindrical basis parameter
         * @param r vector of the r values the psi function will be computed on; cylindrical basis parameter
         * @return the arma::mat of the values of the psi function with parameters m, n and nz computed on all pairs of values of z and r vectors
         */
        arma::mat psi(int m, int n, int nz, arma::vec z, arma::vec r);


        /**
        * @brief Function that returns the Z factors for all possible values of n_z under the given one (0 to nz included)
        * 
        * @param z arma:::vec of the points the function is evaluated on
        * @param nz int quantum number nz maximal value
        *
        * @return the arma::mat of the Z factors of the psi functions
        */
        arma::mat zPart_mat(arma::vec z, int nz);

        /**
        * @brief Function that returns the R factor for all possible values of m and n under the given ones (from 0 to m and 0 to n included)
        * @param r arma:::vec of the points the function is evaluated on
        * @param m int quantum number m maximal value
        * @param n int quantum number n maximal value
        *
        * @return the arma::cube of the R factor of the matching psi function
        */
        arma::cube rPart_cube(arma::vec r, int m, int n);
};

#endif